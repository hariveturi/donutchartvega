Donut Chart (Vega.js)

Configuration
The autosize parameter was set to “fit”

Signals
Start angle provided was 4.71rad and end angle provided was 7.85rad
The innerradius property creates a donut which is set to 310
Padangle creates a separation between arcs of the donut

Data
Data provided in google docs for donut chart as table name-value pairs
Pie transform was used to convert the data values to a pie format data stream

Scales
The color scale was used and the category20 palette was used as seen in google docs to map colors to the pie transform

Marks
Arc marks were mapped to the pie transform using the color palette category20 and rendered to the view for all the values in the data table
Text values such as labels were hardcoded using the “text” mark and their x and y co-ordinates hard coded into the marks object
The small rectangular boxes behind the label showing the percentages were created using the rect mark with a fill of the respective colors.
# Donut Chart - Vega

Gramener - Vega donut chart 
